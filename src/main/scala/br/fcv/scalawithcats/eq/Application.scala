package br.fcv.scalawithcats.eq

import cats.Eq
import cats.instances.int._
import cats.instances.long._
import cats.instances.option._
import cats.syntax.eq._
import cats.syntax.option._

import java.util.Date // for some and none

object Application {
  def ints(): Unit = {
    val eqInt = Eq[Int]
    eqInt.eqv(1, 2)

    println(123 === 123)
    println(123 =!= 125)

    // 123 === ""
    // [error] /home/fcv/projects/personal/cats-sandbox/src/main/scala/br/fcv/scalawithcats/eq/Application.scala:15:13: type mismatch;
    // [error]  found   : String("")
    // [error]  required: Int
    // [error]     123 === ""
    // [error]             ^
    // [error] one error found
  }

  def options(): Boolean = {
    (Some(1): Option[Int]) === (None: Option[Int])
    1.some === none[Int]

    val opt1: Option[Int] = Some(1)
    val opt2: Option[Int] = None

    opt1 === opt2 &&
      opt1 === None &&
      opt1 === Some(1)
  }


  implicit val dateEq: Eq[Date] = (date1, date2) => date1.getTime === date2.getTime

  def cats(): Unit = {
    final case class Cat(name: String, age: Int, color: String)

    val cat1 = Cat("Garfield",   38, "orange and black")
    val cat2 = Cat("Heathcliff", 33, "orange and black")

    val optionCat1: Option[Cat] = Option(cat1)
    val optionCat2: Option[Cat] = Option.empty

    implicit val catEq: Eq[Cat] = _ == _

    println(cat1 === cat2)
    println(optionCat1 === optionCat2)
  }
}

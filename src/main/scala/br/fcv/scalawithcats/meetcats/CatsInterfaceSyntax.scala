package br.fcv.scalawithcats.meetcats

import cats.implicits.{catsStdShowForInt, catsStdShowForString}
import cats.syntax.show._

/**
 * > We can make Show easier to use by importing the interface syntax from cats.syntax.show. This adds an extension
 * > method called show to any type for which we have an instance of Show in scope.
 *
 * @see https://www.scalawithcats.com/dist/scala-with-cats.html#importing-interface-syntax
 */
object CatsInterfaceSyntax {
  val shownInt = 123.show
  // shownInt: String = "123"

  val shownString = "abc".show
  // shownString: String = "abc"
}

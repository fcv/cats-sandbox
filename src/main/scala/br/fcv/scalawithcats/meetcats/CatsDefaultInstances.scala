package br.fcv.scalawithcats.meetcats

import cats.Show
import cats.instances.int.catsStdShowForInt
import cats.instances.string.catsStdShowForString

/**
 * > The cats.instances package provides default instances for a wide variety of types.
 * > We can import these as shown in the table below. Each import provides instances of
 * > all Cats’ type classes for a specific parameter type:
 *
 * @see https://www.scalawithcats.com/dist/scala-with-cats.html#importing-default-instances
 */
object CatsDefaultInstances {
  val showInt = Show.apply[Int]
  val showString: Show[String] = Show.apply[String]
}

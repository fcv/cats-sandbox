package br.fcv.scalawithcats.meetcats

trait Printable[A] {
  def format(a: A): String
}
object Printable {
  def format[A](a: A)(implicit printable: Printable[A]): String = printable.format(a)
  def print[A](a: A)(implicit printable: Printable[A]): Unit = println(format(a))
}

object PrintableInstances {
  implicit val printableString: Printable[String] = identity
  implicit val printableInt: Printable[Int] = i => i.toString
}

object PrintableSyntax {

  implicit class PrintableOps[A](val a: A) extends AnyVal {
    def format(implicit printable: Printable[A]): String = Printable.format(a)
    def print(implicit printable: Printable[A]): Unit = Printable.print(a)
  }
}
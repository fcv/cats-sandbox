package br.fcv.scalawithcats.meetcats

final case class Cat(name: String, age: Int, color: String)

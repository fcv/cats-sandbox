package br.fcv.scalawithcats.meetcats

import cats.Show
import cats.implicits.toShow

import java.util.Date

/**
 * > We can define an instance of Show simply by implementing the trait for a given type.
 *
 * @see https://www.scalawithcats.com/dist/scala-with-cats.html#defining-custom-instances
 */
object CatsCustomInterfaces {

  def main(args: Array[String]): Unit = {
    implicit val dateShow: Show[Date] =
      (date: Date) => s"${date.getTime}ms since the epoch."

    println(new Date().show)
  }
}

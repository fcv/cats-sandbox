package br.fcv.scalawithcats.meetcats

import cats.Show
import cats.syntax.show._
import cats.instances.string.catsStdShowForString
import cats.instances.int.catsStdShowForInt

object ApplicationUsingShow {

  implicit val catShow: Show[Cat] =
    cat =>
      s"${cat.name.show} is a ${cat.age.show} year-old ${cat.color.show} cat."

  def main(args: Array[String]): Unit = {
    val cat = Cat("bob", 5, "Blue")
    println(cat.show)
  }
}

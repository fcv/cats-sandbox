package br.fcv.scalawithcats.meetcats

import PrintableInstances._
import PrintableSyntax._

object ApplicationUsingPrintable {

  implicit val printableCat: Printable[Cat] =
    cat =>
      s"${Printable.format(cat.name)} is a ${Printable.format(cat.age)} year-old ${Printable.format(cat.color)} cat."

  def main(args: Array[String]): Unit = {
    val cat = Cat("bob", 5, "Blue")
    cat.print
  }
}

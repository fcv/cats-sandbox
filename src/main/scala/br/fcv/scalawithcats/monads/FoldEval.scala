package br.fcv.scalawithcats.monads

import cats.Eval

/**
 * 4.6.5 Exercise: Safer Folding using Eval
 *
 * @see https://www.scalawithcats.com/dist/scala-with-cats.html#exercise-safer-folding-using-eval
 */
object FoldEval {

  def foldRight_original[A, B](as: List[A], acc: B)(fn: (A, B) => B): B =
    as match {
      case head :: tail =>
        fn(head, foldRight_original(tail, acc)(fn))
      case Nil =>
        acc
    }

  def foldRight[A, B](as: List[A], acc: B)(fn: (A, B) => B): B =
    foldRightEval(as, Eval.now(acc))((a, b) => b.map(fn(a, _))).value

  def foldRightEval[A, B](as: List[A], acc: Eval[B])(fn: (A, Eval[B]) => Eval[B]): Eval[B] =
    as match {
      case head :: tail =>
        Eval.defer(fn(head, foldRightEval(tail, acc)(fn)))
      case Nil =>
        acc
    }

  def main(args: Array[String]): Unit = {
    val r1 = foldRight((1 to 100000).toList, 0L)(_ + _)
    println(r1)

    val r2 = foldRight(List("a", "b", "c"), "")(_ + _)
    println(r2)
  }
}

package br.fcv.scalawithcats.monads.reader

import cats.data.Reader
import cats.syntax.applicative._ // for pure


object DbReaderExercise {

  final case class Db(usernames: Map[Int, String], passwords: Map[String, String])

  type DbReader[A] = Reader[Db, A]

  def findUsername(userId: Int): DbReader[Option[String]] =
    Reader[Db, Option[String]](db => db.usernames.get(userId))

  def checkPassword(username: String, password: String): DbReader[Boolean] =
    Reader[Db, Boolean](db => db.passwords.get(username).contains(password))

  def checkLogin(userId: Int,
                 password: String): DbReader[Boolean] =
    for {
      maybeUsername <- findUsername(userId)
      passwordOk <- maybeUsername
        .map(checkPassword(_, password))
        .getOrElse(false.pure[DbReader])
    } yield passwordOk
}

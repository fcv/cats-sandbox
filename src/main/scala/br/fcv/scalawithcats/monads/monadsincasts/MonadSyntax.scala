package br.fcv.scalawithcats.monads.monadsincasts

import cats.instances.option._   // for Monad
import cats.instances.list._     // for Monad
import cats.syntax.applicative._ // for pure

import cats.Monad
import cats.syntax.functor._ // for map
import cats.syntax.flatMap._ // for flatMap

/**
 * @see https://www.scalawithcats.com/dist/scala-with-cats.html#monad-syntax
 */
object MonadSyntax {

  1.pure[Option]
  // res5: Option[Int] = Some(1)
  1.pure[List]
  // res6: List[Int] = List(1)

  def sumSquare[F[_] : Monad](f1: F[Int], f2: F[Int]): F[Int] =
    for {
      a <- f1
      b <- f2
    } yield (a * a) + (b * b)

  def main(args: Array[String]): Unit = {
    println(sumSquare(Option(3), Option(4))) // Some(25)
    println(sumSquare(List(1, 2, 3), List(4, 5))) // List(17, 26, 20, 29, 25, 34)

    // Identity monad

    import cats.Id

    println(sumSquare[Id](3, 4)) // 7
  }
}

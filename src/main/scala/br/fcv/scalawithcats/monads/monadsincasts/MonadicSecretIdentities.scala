package br.fcv.scalawithcats.monads.monadsincasts

import br.fcv.scalawithcats.monads.FuncY.Monad

/**
 * 4.3.1 Exercise: Monadic Secret Identities
 *
 * @see https://www.scalawithcats.com/dist/scala-with-cats.html#exercise-monadic-secret-identities
 */
object MonadicSecretIdentities {

  type Id[T] = T

  class IdMonad extends Monad[Id] {
    override def pure[A](value: A): Id[A] = value
    override def flatMap[A, B](value: Id[A])(func: A => Id[B]): Id[B] = func(value)
    // flatMap == map since Id[T] == T
    override def map[A, B](value: Id[A])(func: A => B): Id[B] = func(value)
  }

}

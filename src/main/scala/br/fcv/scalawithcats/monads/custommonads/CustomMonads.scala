package br.fcv.scalawithcats.monads.custommonads

import cats.Monad

import scala.annotation.tailrec

/**
 * 4.10 Defining Custom Monads
 *
 * @see https://www.scalawithcats.com/dist/scala-with-cats.html#defining-custom-monads
 */
object CustomMonads {

  implicit val optionMonad: Monad[Option] = new Monad[Option] {
    override def flatMap[A, B](fa: Option[A])(f: A => Option[B]): Option[B] =
      fa.flatMap(f)

    @tailrec
    override def tailRecM[A, B](a: A)(f: A => Option[Either[A, B]]): Option[B] = {
      f(a) match {
        case Some(Right(b1)) => Some(b1)
        case Some(Left(a1)) => tailRecM(a1)(f)
        case None => None
      }
    }

    override def pure[A](x: A): Option[A] = Option(x)
  }
}

object Usage {
  import cats.syntax.flatMap._
  import cats.syntax.functor._
  import cats.syntax.monad._ // for iterateWhileM

  def main(args: Array[String]): Unit = {
    val r = retryTailRecM(10000)(a => if(a == 0) None else Some(a - 1))
    println(r)
  }

  def retryM[F[_]: Monad, A](start: A)(f: A => F[A]): F[A] =
    start.iterateWhileM(f)(_ => true)

  def retry[F[_]: Monad, A](start: A)(f: A => F[A]): F[A] =
    f(start).flatMap{ a =>
      retry(a)(f)
    }

  def retryTailRecM[F[_]: Monad, A](start: A)(f: A => F[A]): F[A] =
    Monad[F].tailRecM(start){ a =>
      f(a).map(a2 => Left(a2))
    }
}

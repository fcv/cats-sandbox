package br.fcv.scalawithcats.monads

/**
 * 4.1.2 Exercise: Getting Func-y
 *
 * @see https://www.scalawithcats.com/dist/scala-with-cats.html#exercise-getting-func-y
 */
object FuncY {

  trait Monad[F[_]] {
    def pure[A](a: A): F[A]

    def flatMap[A, B](value: F[A])(func: A => F[B]): F[B]

    def map[A, B](value: F[A])(func: A => B): F[B] = {
      // map can always be implemented in terms of flatMap and pure
      flatMap(value)(a => pure(func(a)))
    }
  }
}

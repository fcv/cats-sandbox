package br.fcv.scalawithcats.monads.state

import br.fcv.scalawithcats.monads.state.PostOrderCalculator.{CalcState, evalAll, evalInput, evalOne}
import cats.data.State
import cats.implicits._

/**
 * 4.9.3 Exercise: Post-Order Calculator
 *
 * https://www.scalawithcats.com/dist/scala-with-cats.html#exercise-post-order-calculator
 */
object PostOrderCalculator {

  type CalcState[A] = State[List[A], A]

  def evalOne(sym: String): CalcState[Int] = sym match {
    case IntVal(i) =>
      State[List[Int], Int] { stack => (i :: stack, i) }
    case "+" =>
      operator(_ + _)
    case "-" =>
      operator(_ - _)
    case "/" =>
      operator(_ / _)
    case "*" =>
      operator(_ * _)
    case other =>
      throw new IllegalArgumentException(s"Invalid symbol: '$other'")
  }

  def evalAll(syms: Seq[String]): CalcState[Int] = {
    val states: Seq[CalcState[Int]] = syms.map(evalOne)
    states.foldLeft(State.empty[List[Int], Int]) { case (a, b) => a.flatMap(_ => b) }
  }

  def evalInput(input: String): Int = {
    val symbols = input.trim.split(" +")
    evalAll(symbols.toList).runA(Nil).value
  }

  private def operator[T](f: (T, T) => T): CalcState[T] = {
    State[List[T], T] {
      case a :: b :: tail =>
        val r = f(a, b)
        (r :: tail, r)
      case other =>
        throw new IllegalStateException(s"$other contains an invalid number of operands, expected 2 found ${other.length}")
    }
  }

  object IntVal {
    def unapply(arg: String): Option[Int] = arg.toIntOption
  }
}

object Usage {
  def main(args: Array[String]): Unit = {
    val program: CalcState[Int] = for {
      _    <- evalOne("1")
      _    <- evalOne("2")
      _    <- evalOne("+")
      _    <- evalOne("3")
      ans2 <- evalOne("*")
    } yield ans2

    println(program.runA(Nil).value) // 9

    val biggerProgram = for {
      _   <- evalAll(List("1", "2", "+"))
      _   <- evalAll(List("3", "4", "+"))
      ans <- evalOne("*")
    } yield ans

    println(biggerProgram.runA(Nil).value) // 21

    println(evalInput("  1 21 + 3   4 + *  ")) // 154
    println(evalInput("  1 21 + 3 4 + *")) // 154
  }
}

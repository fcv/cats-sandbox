package br.fcv.scalawithcats.monads.writer

import cats.data.Writer
import cats.implicits.catsSyntaxApplicativeId

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.DurationInt

object ConcurrentFactorial {

  def slowly[A](body: => A) =
    try body finally Thread.sleep(100)

  object Original {
    def factorial(n: Int): Int = {
      val ans = slowly(if(n == 0) 1 else n * factorial(n - 1))
      println(s"fact $n $ans")
      ans
    }
  }

  type Logged[A] = Writer[Vector[String], A]
  import cats.syntax.writer._ // for tell
  import cats.instances.vector._ // for Monoid
  Vector("Message").tell
  41.pure[Logged].map(_ + 1)

  def factorial(n: Int): Logged[Int] = {
    for {
      ans <- {
        if (n == 0) {
          1.pure[Logged]
        } else {
          slowly(factorial(n - 1).map(_ * n))
        }
      }
      _ <- Vector(s"fact $n $ans").tell
    } yield ans
  }

  def main(args: Array[String]): Unit = {
    println(Original.factorial(5))

    val (log, res) = factorial(5).run
    println((log, res))

    val r: Seq[Vector[String]] = Await.result(Future.sequence(Vector(
      Future(factorial(5)),
      Future(factorial(5))
    )).map(_.map(_.written)), 5.seconds)
    println(r)
  }
}

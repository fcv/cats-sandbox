package br.fcv.scalawithcats.monads.monaderror

import cats.MonadError
import cats.instances.either._ // for MonadError

object MonadErrorTypeClass {

  type ErrorOr[A] = Either[String, A]
  val monadError = MonadError[ErrorOr, String]

  val success = monadError.pure(42)
  // success: ErrorOr[Int] = Right(42)
  val failure = monadError.raiseError("Badness")
  // failure: ErrorOr[Nothing] = Left("Badness")

  monadError.handleErrorWith(failure) {
    case "Badness" =>
      monadError.pure("It's ok")

    case _ =>
      monadError.raiseError("It's not ok")
  }
  // res0: ErrorOr[String] = Right("It's ok")
}

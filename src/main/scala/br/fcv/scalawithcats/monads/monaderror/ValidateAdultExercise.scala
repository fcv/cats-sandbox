package br.fcv.scalawithcats.monads.monaderror

import cats.MonadError
import cats.implicits._

import scala.util.Try

object ValidateAdultExercise {

  def validateAdult[F[_]](age: Int)(implicit me: MonadError[F, Throwable]): F[Int] = {
    if (age >= 18) {
      me.pure(age)
    } else {
      me.raiseError(new IllegalArgumentException("Age must be greater than or equal to 18"))
    }
  }

  def main(args: Array[String]): Unit = {
    println(validateAdult[Try](18))
    println(validateAdult[Try](8))

    type ExceptionOr[A] = Either[Throwable, A]
    println(validateAdult[ExceptionOr](-1))
  }
}



package br.fcv.scalawithcats.monoids

trait Semigroup[A] {
  def combine(x: A, y: A): A
}

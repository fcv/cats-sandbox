package br.fcv.scalawithcats.monoids

/**
 * 2.4 Exercise: All Set for Monoids
 * @see https://www.scalawithcats.com/dist/scala-with-cats.html#exercise-all-set-for-monoids
 */
object Sets {

  class Union[T] extends Monoid[Set[T]] {
    override def empty: Set[T] = Set.empty
    override def combine(x: Set[T], y: Set[T]): Set[T] = x ++ y
  }

  class Intersection[T] extends Semigroup[Set[T]] {
    override def combine(x: Set[T], y: Set[T]): Set[T] = x.intersect(y)
  }

  class SymmetricDifference[T] extends Monoid[Set[T]] {
    override def empty: Set[T] = Set.empty
    override def combine(x: Set[T], y: Set[T]): Set[T] = (x union y) diff (x intersect y)
  }
}

package br.fcv.scalawithcats.monoids

/**
 * @see https://www.scalawithcats.com/dist/scala-with-cats.html#exercise-the-truth-about-monoids
 */
object Booleans {
  implicit object And extends Monoid[Boolean] {
    override def empty: Boolean = true
    override def combine(x: Boolean, y: Boolean): Boolean = x && y
  }
  implicit object Or extends Monoid[Boolean] {
    override def empty: Boolean = false
    override def combine(x: Boolean, y: Boolean): Boolean = x || y
  }

  /**
   * f xor f = f
   * f xor t = t
   * t xor f = t
   * t xor t = f
   */
  implicit object Xor extends Monoid[Boolean] {
    override def empty: Boolean = false
    override def combine(x: Boolean, y: Boolean): Boolean = (x && !y) || (!x && y)
  }

  /**
   * f xnor f = t
   * f xnor t = f
   * t xnor f = f
   * t xnor t = t
   */
  implicit object Xnor extends Monoid[Boolean] {
    override def empty: Boolean = true
    override def combine(x: Boolean, y: Boolean): Boolean = (!x && y) && (x && !y)
  }
}
